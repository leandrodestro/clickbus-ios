//
//  Kaissara.swift
//  WLAPP
//
//  Created by Leandro Gava Destro on 17/03/20.
//  Copyright © 2020 Ruan Reis. All rights reserved.
//

import UIKit

struct Kaissara: WhitelabelProtocol {
    
    func getURL() -> String {
        return "https://appitapemirim.wpengine.com/"
    }
    
    func getStatusBarColor() -> UIColor {
        return UIColor.white
    }
    
    func getPrimaryColor() -> UIColor {
        return UIColor(red: 0.93, green: 0.04, blue: 0.16, alpha: 1)
    }
    
    func getStatusBarStyle() -> UIStatusBarStyle {
        return .default
    }
    
}
