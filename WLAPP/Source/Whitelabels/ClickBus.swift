//
//  ClickBus.swift
//  WLAPP
//
//  Created by Ruan Reis on 07/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import UIKit

struct ClickBus: WhitelabelProtocol {
    
    func getURL() -> String {
        return "https://www.clickbus.com.br/"
    }
    
    func getStatusBarColor() -> UIColor {
        return UIColor(red: 0.36, green: 0.08, blue: 0.6, alpha: 1)
    }
    
    func getPrimaryColor() -> UIColor {
        return UIColor(red: 0.36, green: 0.08, blue: 0.6, alpha: 1)
    }
    
    func getStatusBarStyle() -> UIStatusBarStyle {
        return .lightContent
    }
    
}
