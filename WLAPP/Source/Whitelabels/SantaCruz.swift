//
//  SantaCruz.swift
//  WLAPP
//
//  Created by Ruan Reis on 07/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import UIKit

struct SantaCruz: WhitelabelProtocol {
    
    func getURL() -> String {
        return "https://app-santacruz.clickbus.com.br"
    }
    
    func getStatusBarColor() -> UIColor {
        return UIColor.white
    }
    
    func getPrimaryColor() -> UIColor {
        return UIColor(red: 0.93, green: 0.04, blue: 0.16, alpha: 1)
    }
    
    func getStatusBarStyle() -> UIStatusBarStyle {
        return .default
    }
    
}
