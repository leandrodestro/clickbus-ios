//
//  GrupoBelarmino.swift
//  WLAPP
//
//  Created by Leandro Gava Destro on 17/03/20.
//  Copyright © 2020 Ruan Reis. All rights reserved.
//

import UIKit

struct GrupoBelarmino: WhitelabelProtocol {
    
    func getURL() -> String {
        return "https://appbamcaf.wpengine.com/"
    }
    
    func getStatusBarColor() -> UIColor {
        return UIColor.white
    }
    
    func getPrimaryColor() -> UIColor {
        return UIColor(red: 1, green: 0, blue: 0, alpha: 1)
    }
    
    func getStatusBarStyle() -> UIStatusBarStyle {
        return .default
    }
    
}
