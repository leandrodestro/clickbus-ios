//
//  Kaissara.swift
//  WLAPP
//
//  Created by Leandro Gava Destro on 17/03/20.
//  Copyright © 2020 Ruan Reis. All rights reserved.
//

import UIKit

struct Camurujipe: WhitelabelProtocol {
    
    func getURL() -> String {
        return "https://www.camurujipe.com.br/"
    }
    
    func getStatusBarColor() -> UIColor {
        return UIColor.white
    }
    
    func getPrimaryColor() -> UIColor {
        return UIColor(red: 0.10, green: 0.67, blue: 0.72, alpha: 1)
    }
    
    func getStatusBarStyle() -> UIStatusBarStyle {
        return .default
    }
    
}
