//
//  WhitelabelContract.swift
//  WLAPP
//
//  Created by Ruan Reis on 07/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import UIKit

protocol WhitelabelProtocol {
    
    func getURL() -> String
    
    func getPrimaryColor() -> UIColor
    
    func getStatusBarColor() -> UIColor
    
    func getStatusBarStyle() -> UIStatusBarStyle
}
