//
//  UIViewController+UIAlertView.swift
//  WLAPP
//
//  Created by Ruan Reis on 18/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func closeApp() {
        UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
    }
    
    func showAlert(title: String, message: String, label: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: label, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showAlertWithAction(title: String, message: String, label: String, action: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: label, style: .default, handler: action))
        self.present(alert, animated: true)
    }
}
