//
//  Connectivity.swift
//  WLAPP
//
//  Created by Ruan Reis on 18/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnected: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
