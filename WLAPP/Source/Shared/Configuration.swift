//
//  Switcher.swift
//  WLAPP
//
//  Created by Ruan Reis on 07/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import Foundation

class Configuration {
    
    private static var instance: Configuration {
        return Configuration(whitelabel: BuildSelector.target)
    }
    
    let whitelabel: WhitelabelProtocol
    
    private init(whitelabel: WhitelabelProtocol) {
        self.whitelabel = whitelabel
    }
    
    class func shared() -> Configuration {
        return instance
    }
    
}
