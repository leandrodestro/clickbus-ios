//
//  BuildSelector.swift
//  WLAPP
//
//  Created by Ruan Reis on 07/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import Foundation

struct BuildSelector {
    
    static var target: WhitelabelProtocol {
        #if CLICKBUS_WHITELABEL
        return ClickBus()
        #elseif SANTACRUZ_WHITELABEL
        return SantaCruz()
        #elseif KAISSARA_WHITELABEL
        return Kaissara()
        #elseif CAMURUJIPE_WHITELABEL
        return Camurujipe()
        #elseif GRUPOBELARMINO_WHITELABEL
        return GrupoBelarmino()
        #endif
    }
    
}
