//
//  ViewController.swift
//  WLAPP
//
//  Created by Ruan Reis on 03/10/19.
//  Copyright © 2019 Ruan Reis. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var container: UIView!
    
    lazy var loading: UIActivityIndicatorView = {
        return UIActivityIndicatorView()
    }()
    
    lazy var whitelabel: WhitelabelProtocol = {
        return Configuration.shared().whitelabel
    }()
    
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        return WKWebView(frame: .zero, configuration: webConfiguration)
    }()
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return whitelabel.getStatusBarStyle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        setupLoading()
        setupConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startWhitelabel()
    }
    
    func setupWebView() {
        // inject JS to capture console.log output and send to iOS
        let source = "function captureLog(msg) { window.webkit.messageHandlers.logHandler.postMessage(msg); } window.console.log = captureLog;"
        let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(script)
        // register the bridge script that listens for the output
        webView.configuration.userContentController.add(self, name: "logHandler")
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        container.addSubview(webView)
    }
    
    func setupConstraints() {
        container.addConstraints([alignConstraintIn(.width), alignConstraintIn(.height)])
    }
    
    func alignConstraintIn(_ attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: webView, attribute: attribute, relatedBy: .equal,
                                  toItem: container, attribute: attribute, multiplier: 1, constant: 0)
    }
    
    func setupLoading() {
        loading.style = .gray
        loading.center = view.center
        loading.hidesWhenStopped = true
        loading.color = whitelabel.getPrimaryColor()
        loading.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
        loading.startAnimating()
        container.addSubview(loading)
    }
    
    func startWhitelabel() {
        guard Connectivity.isConnected else {
            showLostConnectionMessage(completation: startWhitelabel)
            return
        }
        
        print(whitelabel.getURL())
        if let url = URL(string: whitelabel.getURL()) {
            let request = URLRequest(url: url)
            webView.load(request)
            webView.allowsBackForwardNavigationGestures = true
            webView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func openNewTabAsWebView(navigation: WKNavigationAction) {
        guard Connectivity.isConnected else {
            showLostConnectionMessage()
            return
        }
        
        loading.startAnimating()
        webView.load(navigation.request)
    }
    
    func showLostConnectionMessage() {
        let title = NSLocalizedString("lostConnectionTitle", comment: "alert title")
        let message = NSLocalizedString("lostConnectionMessage", comment: "alert message")
        showAlert(title: title, message: message, label: "OK")
    }
    
    func showLostConnectionMessage(completation: @escaping () -> Void) {
        let label = NSLocalizedString("tryAgain", comment: "button label text")
        let title = NSLocalizedString("lostConnectionTitle", comment: "alert title")
        let message = NSLocalizedString("lostConnectionMessage", comment: "alert message")
        
        showAlertWithAction(title: title, message: message, label: label, action: { _ in
            completation()
        })
    }
    
    func showErrorConnectionAlert() {
        let title = NSLocalizedString("errorConnectionTitle", comment: "alert title")
        let message = NSLocalizedString("errorConnectionMessage", comment: "alert message")
        
        showAlertWithAction(title: title, message: message, label: "OK", action: { _ in
            self.closeApp()
        })
    }
}

extension WebViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if !Connectivity.isConnected {
            loading.stopAnimating()
            showLostConnectionMessage()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loading.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loading.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction,
                 windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        let isMainFrame = navigationAction.targetFrame?.isMainFrame ?? false
        
        if !isMainFrame  {
            openNewTabAsWebView(navigation: navigationAction)
        }
        
        return nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let response = navigationResponse.response as? HTTPURLResponse, response.statusCode >= 500 {
            showErrorConnectionAlert()
        }
        
        decisionHandler(.allow)
    }
}


extension WebViewController: WKScriptMessageHandler {

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "logHandler" {
            print("######### LOG: \(message.body)")
        }
    }
    
}
